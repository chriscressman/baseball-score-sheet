Baseball score sheet
====================

A simple baseball score sheet, available in [HTML](baseball-score-sheet.html) and [PDF](baseball-score-sheet.pdf).
